<?php

namespace Drupal\fpdi_print\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Fpdi print form.
 */
class ValidateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'fpdi_print_validate';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $pdf = $this->getRequest()->get('pdf');
    $form['container'] = [
      '#type' => 'container',
      'pdf' => [
        '#type' => 'textfield',
        '#title' => $this->t('Pdf path'),
        '#required' => TRUE,
        '#default_value' => $pdf,
        '#wrapper_attributes' => [
          'class' => ['layout-column', 'layout-column--half', 'col-6'],
        ],
        '#attributes' => [
          'placeholder' => 'Ex: /sites/default/files/template.pdf',
          'data-pdf' => $pdf,
        ],
        '#description' => ' ',
      ],
      'yaml' => [
        '#type' => 'textarea',
        '#title' => $this->t('Yaml Validation'),
        '#wrapper_attributes' => [
          'class' => ['layout-column', 'layout-column--half', 'col-6'],
        ],
        '#attributes' => [
          'data-editor' => 'edit-yaml',
          'class' => ['yaml-editor'],
        ],
        '#description' => $this->t('<pre>- x: 10
  y: 10
  html: [site:name]
- x: 20
  y: 10
  text: {{ field_name }}
- x: 20
  y: 40
  width: 50
  height: 10
  image: /sites/default/files/images.png
</pre>'),
      ],
      '#attributes' => [
        'class' => ['row', 'layout-row', 'clearfix'],
      ],
    ];

    $form['#attached']['library'][] = 'fpdi_print/fpdi_print';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->messenger()->addStatus($this->t('Reload validation form.'));
  }

}
