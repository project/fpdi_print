<?php

namespace Drupal\fpdi_print\Plugin\views\area;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views\Attribute\ViewsArea;
use Drupal\views\Plugin\views\area\TokenizeAreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area handler for a pdf button.
 *
 * @ingroup views_area_handlers
 */
#[ViewsArea("fpdi_print_views_link")]
class FpdiPrintViewsLink extends TokenizeAreaPluginBase {

  /**
   * Constructs a Handler object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected FileSystemInterface $fileSystem, protected ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('file_system'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['position_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Position configuration'),
      '#description' => $this->t('YAML array format, for example:') . '<br/>' . "
      <pre>- x: 10
  y: 10
  text: \"[site:name]\"
- x: 20
  y: 10
  text: \"{{ field_name }}\"</pre>
  or Form filling
  <pre>- form: \"address\"
  text:  \"[site:name]\"
- form: \"name\"
  text: \"{{ field_name }}
</pre>",
      '#default_value' => $this->options['position_text'],
      '#attributes' => [
        'data-editor' => 'edit-yaml',
        'id' => 'edit-yaml',
        // @todo add yaml editor to work.
        // 'class' => ['yaml-editor'],
      ],
    ];
    $form['#attached']['library'][] = 'fpdi_print/fpdi_print';

    $form['print_logo'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use logo, slogan for the header'),
      '#default_value' => $this->options['print_logo'],
    ];

    $form['link_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#required' => TRUE,
      '#default_value' => $this->options['link_text'],
    ];
    $path = $this->fileSystem->realpath($this->configFactory->get('system.file')->get('default_scheme') . "://");
    $form['file_pdf'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PDF document (version 1.4) template path'),
      '#description' => $this->t('It does not work with version 1.5 later, <a href="https://docupub.com/pdfconvert/">convert to acrobat 5 (PDF 1.4)</a>. <br/>Your system path: %path', ['%path' => $path]),
      '#attributes' => ['placeholder' => 'Ex: sites/default/files/template.pdf'],
      '#default_value' => $this->options['file_pdf'],
    ];
    $form['file_pdf']['#description'] .= '<br>';
    $form['file_pdf']['#description'] .= $this->t('See more for <a href="http://www.fpdf.org/en/script/script93.php">PDF Form Filling</a>');
    $form['file_pdf']['#description'] .= '<br>';
    $form['file_pdf']['#description'] .= $this->t('Check position and validation yaml <a href="@check" target="_blank">Pdf position & validate yaml</a>', [
      '@check' => Url::fromRoute('fpdi_print.validate', [], [
        'query' => ['pdf' => $this->options['file_pdf']],
      ])->toString(),
    ]);

    $form['file_pdf_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Save PDF to name'),
      '#description' => $this->t('Leave blank to view the pdf with the browser. If you set the file name, it will download the pdf, If you set path + file name, it will be saved in the path and downloaded. <br/> The token can be used for filename'),
      '#default_value' => $this->options['file_pdf_name'],
    ];

    $displays = $this->view->displayHandlers->getConfiguration();
    $display_options = [];
    foreach ($displays as $display_id => $display_info) {
      $display_options[$display_id] = $display_info['display_title'];
    }
    $form['display_id'] = [
      '#type' => 'select',
      '#title' => $this->t('View Display'),
      '#options' => $display_options,
      "#empty_option" => $this->t('- Select -'),
      '#default_value' => $this->options['display_id'],
      '#required' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $route_params = [
      'view_name' => $this->view->storage->id(),
      'display_id' => $this->options['display_id'],
      'option_id' => $this->areaType . '-' . $this->options['id'],
    ];
    return [
      '#type' => 'link',
      '#title' => $this->options['link_text'],
      '#attributes' => ['class' => ['print-file-pdf']],
      '#url' => Url::fromRoute('fpdi_print.view', $route_params, [
        'query' => $this->view->getExposedInput() + ['view_args' => $this->view->args],
      ]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['position_text'] = ['default' => ''];
    $options['file_pdf'] = ['default' => ''];
    $options['file_pdf_name'] = ['default' => ''];
    $options['print_logo'] = ['default' => FALSE];
    $options['link_text'] = ['default' => $this->t('Download PDF')];
    $options['display_id'] = ['default' => $this->view->current_display];
    return $options;
  }

}
