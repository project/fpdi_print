<?php

namespace Drupal\fpdi_print;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use setasign\Fpdi\PdfParser\StreamReader;
use setasign\Fpdi\Tcpdf\Fpdi;

/**
 * Get field form pdf service.
 */
class FormFields {

  /**
   * FileDownloadController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type mananager service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   The stream wrapper manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration service.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager, protected StreamWrapperManagerInterface $streamWrapperManager, protected AccountInterface $currentUser, protected ConfigFactoryInterface $configFactory) {
  }

  /**
   * {@inheritdoc}
   */
  public function get($pdfFile) {
    $pdf = new Fpdi();
    $pageCount = $pdf->setSourceFile($pdfFile);
    $formFields = [];
    foreach (range(1, $pageCount) as $pageNo) {
      $template = $pdf->importPage($pageNo);
      $parser = new StreamReader($pdf->getPdfParser());
      $fields = $parser->getFormFields($template);
      if (!empty($fields)) {
        if (empty($formFields[$pageNo])) {
          $formFields[$pageNo] = [];
        }
        foreach ($fields as $fieldName => $fieldValue) {
          $formFields[$pageNo][] = $fieldName;
        }
      }
    }
    return $formFields;
  }

}
