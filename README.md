# PDF printing with template

Module uses library FPDI Generate a PDF file for printing based on a template.
You can import pages from existing PDF documents.

## How to use

Install module fpdi_print with composer require drupal/fpdi_print.
It will add libraries tcpdf and fpdi to vendor
 - Create view page
 - Add Pdf Print (global) to Header or Footer Views page
 - Fill all the configuration
 - Path to pdf source file - version 1.4.
> Check your version pdf before upload, it doesn't work with version 1.5 or 1.6
add position x,y, text (format array yaml), check validation yaml before paste
 - If you leave position configuration empty. It'll print contents of the view.
You can set pdf "Global area custom text" from views header footer
 - Go to page /fpdi-print/validate to check pdf and validate Yaml

## For developper
There is hook fpdi_print_views for alter data positions before generate.
you can call service <strong>fpdi_print.print_builder</strong><br/>
Format of positions :
page number => [ x , y , (text|html|image), height , width]
the page number must start with 1 (NOT BEGIN WITH 0)

```
$positions = [
 '1' => [
     [ 'x'=> 10, 'y'=>20, 'text'=>'Show text in page 1'],
     [ 'x'=> 15, 'y'=>40, 'height'=>50 , 'width'=>50, 'image'=>101
     //you can add fid for image],
     [ 'x'=> 15, 'y'=>50, 'image'=>'/path/to/image' // add image path],
     [ 'x'=> 15, 'y'=>60, 'image'=>'<img src="/path/to/image1.png"/>
     <img src="/path/to/image2.jpg"/>'// add html multi images],
],
 '2' => [
     [ 'x'=> 10, 'y'=>20, 'height'=> 10,
     'html'=>'Show <b>html</b>\n in page 2' ],
]
]


    $pdf = \Drupal::service('fpdi_print.print_builder')
    ->getPDF($positions, $filePdfTemplate);

//you can save pdf to file with $pdf->Output('I','yourFileName.pdf');
//or show in directly to browser
    $response = new StreamedResponse();
    return $response->setCallback(function () use ($pdf) {
      ob_clean();
      $handle = fopen('php://output', 'w+');
      $pdf->Output();
      fclose($handle);
    });

```
