(function ($, Drupal, once) {
  Drupal.behaviors.pdfPosition = {
    attach: function (context) {
      let pdf = once('pdf-load', '#edit-pdf', context);
      if(pdf){
        pdf.forEach(function (pdfPath) {
          function formatUrl(url) {
            return (!url.startsWith('http') && !url.startsWith('/')) ? '/' + url : url;
          }
          let url = formatUrl($(pdfPath).val());
          if (url) {
            loadAllPages(url);
          }
          $(pdfPath).on('change', function () {
            let url = formatUrl($(this).val());
            if (url) {
              loadAllPages(url);
            }
          });
        });
      }

      $(once('json-yaml-editor', '.yaml-editor', context)).each(function () {
        let editorID = $(this).data('editor');
        if(!$('#'+editorID)) {
          editorID = $(this).attr('id');
        }
        if (ace) {
          let $textarea = $(this);
          let editor = ace.edit(editorID);
          let position = `- text: "first line"\n  x: 10\n  y: 10`;

          if($textarea.val() !== ''){
            position = $textarea.val();
          }
          editor.setValue(position);
          editor.getSession().setMode( "ace/mode/yaml" );
          editor.focus();
          editor.getSession().setTabSize(2);
          editor.setTheme('ace/theme/chrome');
          editor.setOptions({
            maxLines: 30,
            minLines: 100,
            hasCssTransforms: true,
            autoScrollEditorIntoView: true,
          });

          setPosition(position, editor);
          // Update Drupal textarea value.
          editor.getSession().on('change', function () {
            let yamlString = editor.getSession().getValue();
            $textarea.val(yamlString);
            if(yamlString != '') {
              setPosition(yamlString, editor);
            }
          });
        }
      });
    }
  };
  function setPosition(yamlString, editor) {
    let pages = [];
    if (localStorage.getItem('pages')) {
      pages = JSON.parse(localStorage.getItem('pages'));
    }
    try {
      let labels = jsyaml.load(yamlString);

      let container = $('#edit-pdf--description');
      container.find('.draggable').remove();
      let ratio = 2.83464567;
      labels.forEach(function(label, index) {
        let x = label.x ?? 10;
        let y = label.y ?? 20 * index;
        let globalX = label.globalX ?? x * ratio;
        let globalY = label.globalY ?? y * ratio;
        let div = $('<div class="draggable"></div>')
          .css({
            left: globalX + 'px',
            top: globalY + 'px'
          })
          .appendTo(container)
          .draggable({
            containment: "parent",
            stop: function(event, ui) {
              let localPosition = findPageAndLocalY(pages, ui.position.top);
              label.globalX = ui.position.left;
              label.x = ui.position.left / ratio;
              label.globalY = ui.position.top;
              label.y = localPosition.y / ratio;
              label.page = localPosition.page;
              let position = jsyaml.dump(labels);
              editor.setValue(position);
            }
          });
        if(label.text) {
          div.text(label.text);
        }
        if(label.html) {
          div.html(label.html);
        }
        if(label.image) {
          let height = label.height ? label.height * ratio : '';
          let width = label.width ? label.width * ratio : '';
          let img = `<img src="${label.image}" height="${height}" width="${width}"/>`;
          div.html(img);
        }
      });
    } catch (e) {
      console.error(Drupal.t('YAML is not validate:'), e.message);
    }
  }

  function loadAllPages(url) {
    let { pdfjsLib } = globalThis;
    pdfjsLib.GlobalWorkerOptions.workerSrc = '//cdnjs.cloudflare.com/ajax/libs/pdf.js/4.2.67/pdf.worker.min.mjs';
    let loadingTask = pdfjsLib.getDocument(url);
    loadingTask.promise.then(function(pdf) {
      const numPages = pdf.numPages;
      let pages = [];
      let maxWidth = 200;
      let container = document.getElementById('edit-pdf--description');
      for(let pageNumber = 1; pageNumber <= numPages; pageNumber++) {
        pdf.getPage(pageNumber).then(function(page) {
          let scale = 1;
          let viewport = page.getViewport({scale: scale});

          // Create a new canvas element for each page to prevent overlap
          let canvas = document.createElement('canvas');
          canvas.style.display = 'block';  // Ensure each page is on a new line
          let context = canvas.getContext('2d');
          document.body.appendChild(canvas);

          canvas.height = viewport.height;
          canvas.width = viewport.width;
          if(canvas.width > maxWidth) {
            maxWidth = canvas.width;
            container.style.width = maxWidth + 'px';
          }
          pages.push({width: canvas.width, height: canvas.height});
          container.appendChild(canvas);
          let renderContext = {
            canvasContext: context,
            viewport: viewport
          };
          let renderTask = page.render(renderContext);
          renderTask.promise.then(function () {
            localStorage.setItem('pages', JSON.stringify(pages));
          }).catch(function(err) {
            console.error(`Error rendering page ${pageNumber}:`, err);
          });
        }).catch(function(err) {
          console.error(`Error loading page ${pageNumber}:`, err);
        });
      }
      return pages;
    }, function(reason) {
      console.error('PDF loading error:', reason);
    });
  }
  function findPageAndLocalY(pages, globalY) {
    let accumulatedHeight = 0;
    for (let i = 0; i < pages.length; i++) {
      accumulatedHeight += pages[i].height;
      if (accumulatedHeight > globalY) {
        const page = i + 1;
        const localY = globalY - (accumulatedHeight - pages[i].height);
        return { page, y: localY };
      }
    }

    return { page: 0, y: 0 };
  }

}(jQuery, Drupal, once));
